/**
 * This package provides the file monitoring service for file and directory changes
 * using the Apache java library.
 */
/**
 * @author Minh-Long
 *
 */
package com.cloudette.filemonitor;