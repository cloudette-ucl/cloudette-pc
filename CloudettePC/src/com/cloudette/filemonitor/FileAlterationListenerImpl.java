package com.cloudette.filemonitor;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import com.cloudette.filesender.FileSender;
import com.cloudette.filesender.SendQueue;

public class FileAlterationListenerImpl implements FileAlterationListener {

	@Override
	public void onStart(final FileAlterationObserver observer) {
	
	}

	@Override
	public void onDirectoryCreate(final File directory) {
		System.out.println(directory.getAbsolutePath() + " was created.");
		
	}

	@Override
	public void onDirectoryChange(final File directory) {
		System.out.println(directory.getAbsolutePath() + " was modified");
		
	}

	@Override
	public void onDirectoryDelete(final File directory) {
		System.out.println(directory.getAbsolutePath() + " was deleted.");
		System.out.println("Must remove folder from database.");
	}

	@Override
	public void onFileCreate(final File file) {
		System.out.println(file.getAbsoluteFile() + " was created.");
		System.out.println("Must add this file to SkyDrive.");
		SendQueue.addToQueue(file);
		
	}

	@Override
	public void onFileChange(final File file) {
		System.out.println(file.getAbsoluteFile() + " was modified.");
		
	}

	@Override
	public void onFileDelete(final File file) {
		System.out.println(file.getAbsoluteFile() + " was deleted.");
	}

	@Override
	public void onStop(final FileAlterationObserver observer) {
	
	}

}
