package com.cloudette.filesender;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;

public class FileSender {
	
	public static void sendFile(File file) throws IOException
	{
		System.out.println("Starting to send...");
		Socket clientSocket = new Socket("192.168.42.1",9878);
		
		ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
		ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
		oos.writeObject(file.getName());
		
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[100];
		int bytesRead = 0;
		
		while((bytesRead = fis.read(buffer)) > 0)
		{
			oos.writeObject(bytesRead);
			oos.writeObject(Arrays.copyOf(buffer, buffer.length));
		}
		
		oos.close();
		ois.close();
		clientSocket.close();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		System.out.println("File sent successfully.");
		return;
	}

}
