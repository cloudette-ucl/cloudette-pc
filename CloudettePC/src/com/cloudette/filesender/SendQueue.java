package com.cloudette.filesender;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class SendQueue extends Thread {
	
	private static BlockingQueue<File> queue = new LinkedBlockingQueue<File>();
	
	public synchronized static void addToQueue(File inputFile)
	{
		queue.add(inputFile);
	}
	
	public void run()
	{
		File file = null;
		while(true)
		{
		
				try {
					file = queue.take();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				try {
					FileSender.sendFile(file);
				} catch (IOException e) {
				}
			
		}
	}

}
