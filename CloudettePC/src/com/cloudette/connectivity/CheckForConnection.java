package com.cloudette.connectivity;

import java.io.IOException;

public class CheckForConnection {
	
	public static boolean checkConnectionToCloudette() throws IOException, InterruptedException
	{
		Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 192.168.42.1");
		int returnVal = p1.waitFor();
		if (returnVal == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
