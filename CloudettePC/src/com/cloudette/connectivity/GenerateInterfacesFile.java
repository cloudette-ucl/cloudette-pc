package com.cloudette.connectivity;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class GenerateInterfacesFile {
	
	public void generateInterfacesFile(String SSID, String passkey) throws IOException
	{
		//generates an interfaces setting file using the SSID and passkey information given.
		Writer interfacesOutput = null;
		String separator = System.getProperty("line.separator");
		File newInterfacesFile = new File ("interfaces");
		interfacesOutput = new BufferedWriter(new FileWriter(newInterfacesFile));
		interfacesOutput.write("auto lo");
		interfacesOutput.write(separator+separator+"iface lo inet loopback");
		interfacesOutput.write(separator+"iface eth0 inet dhcp");
		interfacesOutput.write(separator+separator+"allow-hotplug wlan0");
		interfacesOutput.write(separator+separator+"iface wlan0 inet static");
		interfacesOutput.write(separator+"\taddress 192.168.42.1");
		interfacesOutput.write(separator+"\tnetmask 255.255.255.0");
		interfacesOutput.write(separator+separator+"allow-hotplug wlan1");
		interfacesOutput.write(separator+"auto wlan1");
		interfacesOutput.write(separator+separator+"iface wlan1 inet dhcp");
		interfacesOutput.write(separator+"\twpa-ssid \"" + SSID + "\"");
		interfacesOutput.write(separator+"\twpa-psk \"" + passkey + "\"");
		interfacesOutput.write(separator+separator+"up iptables-restore < /etc/iptables.ipv4.nat");
		interfacesOutput.close();
	}

}
