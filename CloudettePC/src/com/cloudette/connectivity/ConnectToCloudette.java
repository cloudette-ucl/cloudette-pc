package com.cloudette.connectivity;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.IllegalBlockingModeException;
import java.util.Arrays;

public class ConnectToCloudette {
	
	public void connect() throws UnknownHostException, IOException, InterruptedException
	{
		String command;
		String response;
		
		Socket clientSocket = new Socket("192.168.42.1",6789); //connecting to the Cloudette
		clientSocket.setKeepAlive(true);
		
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
//		if (ping() == true)
//		{
//			System.out.println("Cloudette has successfully been connected.");
//			inputSkyDriveAccessToken();
//		}

			//System.out.println("no");
			//first command, get scan information from the Pi.
			command = "SCAN";
			outToServer.writeBytes(command + '\n');
			outToServer.flush();
			//wait two seconds to give the Pi a chance to write to the socket. If a pipe error occurs, we can increase the sleep.
			Thread.sleep(2000);
			
			//read response from socket.
			response = inFromServer.readLine();
			response = response.replace("NEWLINE", "\n");
			
			//parses the information obtained and puts them in ArrayLists
			ParseScanInformation psi = new ParseScanInformation();
			psi.parseInformation(response);
			
			//close the socket
			clientSocket.close();
			
			//get user input from terminal.
			System.out.println("CONNECTION UTILITY");
			System.out.println("Here are the access points you can connect to: ");
			psi.printInfo();
			System.out.println("Type in the number of the access point you wish to connect to: ");
			System.out.println("If your access point is not here, type 'refresh' to search again.");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String choice = null;
			try {
				choice = br.readLine();
				
			} catch (Exception e) {
				System.out.println("Sorry, you have entered an invalid choice. Please try again.");
				System.exit(1);
			}
			if (choice.equals("refresh"))
			{
				connect();
				return;
			}
			else
			{
				try {
					if (Integer.parseInt(choice) < 1 || Integer.parseInt(choice) > psi.SSID.size())
					{
						System.out.println("Sorry, you have entered an invalid choice. Please try again.");
						System.exit(1);
					}
				} catch (NumberFormatException e) {
					System.out.println("Sorry, you have entered an invalid choice. Please try again.");
					System.exit(1);
				}
			}
			System.out.println("You wish to connect to " + psi.SSID.get(Integer.parseInt(choice) - 1) + "\nPlease enter your passkey for this network.");
			String passkey = br.readLine();
			System.out.println("We are now connecting you to your chosen network, please wait...");
			
			//generate an interfaces file with the user's network and passkey.
			GenerateInterfacesFile gif = new GenerateInterfacesFile();
			gif.generateInterfacesFile(psi.SSID.get(Integer.parseInt(choice)-1), passkey);
			//once file is generated, open a new socket and send command to Cloudette
			
			//////second phase
			Socket clientSocket2 = new Socket("192.168.42.1",6789); //connecting to the Cloudette
			clientSocket2.setKeepAlive(true);
			
			DataOutputStream outToServer2 = new DataOutputStream(clientSocket2.getOutputStream());
			
			command = "CHOSENSSID";
			outToServer2.writeBytes(command + '\n');
			outToServer2.flush();
			outToServer2.close();
	
			Thread.sleep(2000);
			clientSocket2.close();
			
			Socket clientSocket3 = new Socket("192.168.42.1",9876);
			
			File file = new File("interfaces");
			ObjectInputStream ois = new ObjectInputStream(clientSocket3.getInputStream());
			ObjectOutputStream oos = new ObjectOutputStream(clientSocket3.getOutputStream());
			oos.writeObject(file.getName());
			
			FileInputStream fis = new FileInputStream(file);
			byte[] buffer = new byte[100];
			int bytesRead = 0;
			
			while((bytesRead = fis.read(buffer)) > 0)
			{
				oos.writeObject(bytesRead);
				oos.writeObject(Arrays.copyOf(buffer, buffer.length));
			}
			
			oos.close();
			ois.close();
			clientSocket3.close();
			System.out.println("Cloudette now has your connection settings and will attempt to connect to the internet.");
			for(int i = 0; i < 4; i ++)
			{
				if(ping() == true)
				{
					System.out.println("You have successfully connected!");
					break;
				}
			}
			inputSkyDriveAccessToken();
			
		
		
	}

	public void inputSkyDriveAccessToken() throws UnknownHostException, IOException,
			SocketException {
		String command;
		System.out.println("----------------------");
		////////
		System.out.println("Please enter your SkyDrive Access Token");
		String accessToken = new String();
		Socket clientSocket4 = new Socket("192.168.42.1", 6789);
		clientSocket4.setKeepAlive(true);
		
		DataOutputStream outToServer3 = new DataOutputStream(clientSocket4.getOutputStream());
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		
		accessToken = br2.readLine();
		command = accessToken;
		outToServer3.writeBytes(command + '\n');
		outToServer3.flush();
		outToServer3.close();
		clientSocket4.close();
	}
	
	public boolean ping() throws IOException, InterruptedException
	{
		Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
		int returnVal = p1.waitFor();
		if (returnVal == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
