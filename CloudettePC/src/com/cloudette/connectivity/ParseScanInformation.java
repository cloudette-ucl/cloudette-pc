package com.cloudette.connectivity;
import java.util.ArrayList;


public class ParseScanInformation {
	
	//at the moment, only supports up to 9 access points.
	//need to see what happens if more than 9 are found...whether this crashes.
	public int numberOfAccessPoints = 1;
	
	//ArrayList which has the signal strengths of each access point. Value is out of 70. Can re-calculate to obtain a percentage.
	//Use this later to organise list of access points by strength.
	ArrayList<String>Quality = new ArrayList<String>();
	
	//ArrayList which has the names of the access points, also known as SSIDs
	ArrayList<String>SSID = new ArrayList<String>();
	
	public void parseInformation(String input)
	{
		System.out.println(input);
		getNumberOfCells(input);
		getQuality(input);
		getSSIDs(input);
	}
	
	public void printInfo()
	{
		for(int i = 0; i < SSID.size(); i ++)
		{
			System.out.print(i+1 + ". - ");
			System.out.println(SSID.get(i));
		}
	}
	
	private void getSSIDs(String input)
	{
		StringBuilder ssid = new StringBuilder();
		for (int k = 0; k < numberOfAccessPoints-1; k ++)
		{
			int i = input.indexOf("ESSID");
			for(int j = i; j < input.length(); j ++)
			{
				if (input.charAt(j) == '\n')
				{
					input = input.substring(j, input.length());
					SSID.add(ssid.toString());
					ssid.delete(0, ssid.length());
					break;
				}
				else
				{
					ssid.append(input.charAt(j));
				}
			}
		}
		
		for(int l = 0; l < SSID.size(); l ++)
		{
			String str = SSID.get(l);
			str = str.replace("ESSID:", "");
			str = str.substring(1, str.length()-1);
			SSID.set(l, str);
		}
	}
	
	private void getNumberOfCells(String input)
	{
		while (true)
		{
			if (input.contains("Cell 0" + numberOfAccessPoints))
			{
				numberOfAccessPoints++;
			}
			else if (input.contains("Cell " + numberOfAccessPoints))
			{
				numberOfAccessPoints++;
			}
			else
			{
				break;
			}
		}
	}
	
	private void getQuality(String input)
	{
		StringBuilder quality = new StringBuilder();
		for (int k = 0; k < numberOfAccessPoints-1; k ++)
		{
			int i = input.indexOf("Quality");
			for(int j = i; j < input.length(); j ++)
			{
				if (input.charAt(j) == ' ')
				{
					input = input.substring(j, input.length());
					Quality.add(quality.toString());
					quality.delete(0, quality.length());
					break;
				}
				else
				{
					quality.append(input.charAt(j));
				}
			}
		}
		
		for(int l = 0; l < Quality.size(); l ++)
		{
			String str = Quality.get(l);
			str = str.replace("Quality=", "");
			str = str.replace("/70", "");
			Quality.set(l, str);
		}
	}
	
	

}
