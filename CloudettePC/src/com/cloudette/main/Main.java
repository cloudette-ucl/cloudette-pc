package com.cloudette.main;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.cloudette.connectivity.CheckForConnection;
import com.cloudette.connectivity.ConnectToCloudette;
import com.cloudette.filemonitor.FileMonitor;
import com.cloudette.filesender.SendQueue;

public class Main {
	
	public static void main (String args[])
	{
		Main main = new Main();
		main.start();
	}
	
	private void start()
	{
		System.out.println("Welcome to CLOUDETTE\n----------------\nPlease wait...");
		while(true)
		{
			try {
				if (CheckForConnection.checkConnectionToCloudette() == true)
				{
					break;
				}
			} catch (IOException | InterruptedException e) {
			}
		}
		//establish a connection
		ConnectToCloudette ctc = new ConnectToCloudette();
		try {
			if(ctc.ping() == true)
			{
				ctc.inputSkyDriveAccessToken();
			}
			else
			{
				try {
					ctc.connect();
					} 
				catch (IOException | InterruptedException e) {
				e.printStackTrace();
				}
			}
		} 
		catch (IOException | InterruptedException e) {
		}
		//start file monitor
		
		SendQueue q = new SendQueue();
		q.start();
		FileMonitor fm = new FileMonitor();
		fm.start();
	}

}
